# caf_jar

#### 项目介绍
此为符合restful规范的框架项目，需要打包为jar包使用。

#### 软件架构



#### 安装教程

1. 将此工程打包为jar包
2. 新项目引用此工程的jar
3. com.cqt.a8.res.AbstractCafWebApplicationInitializer为启动类，要引用此jar包，需要继承此类。

#### 使用说明
请参考demo示例：https://gitee.com/wetoget/caf_demo

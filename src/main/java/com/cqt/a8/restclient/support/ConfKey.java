package com.cqt.a8.restclient.support;

/**
 * The interface Conf key.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public interface ConfKey {
    /**
     * Gets key.
     *
     * @return the key
     */
    String getKey();

    /**
     * Gets default value.
     *
     * @return the default value
     */
    String getDefaultValue();
}

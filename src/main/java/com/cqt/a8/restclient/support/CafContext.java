package com.cqt.a8.restclient.support;

import java.util.HashMap;
import java.util.Map;

/**
 * The type Caf context.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class CafContext {

    /**
     * The constant LOCAL_REQUEST_ID.
     */
    public static final String LOCAL_REQUEST_ID = "REQUEST_ID";
    private final static ThreadLocal<HashMap> LOCAL = new ThreadLocal<>();

    /**
     * Gets local.
     *
     * @return the local
     */
    public static HashMap getLocal() {
        HashMap hashMap = LOCAL.get();
        if (hashMap == null) {
            hashMap = new HashMap();
            LOCAL.set(hashMap);
        }
        return hashMap;
    }

    /**
     * Sets local.
     *
     * @param local the local
     */
    public static void setLocal(HashMap local) {
        LOCAL.set(local);
    }

    /**
     * Get t.
     *
     * @param <T>   the type parameter
     * @param key   the key
     * @param clazz the clazz
     * @return the t
     */
    public static <T> T get(String key, Class<T> clazz) {
        return (T) getLocal().get(key);
    }

    /**
     * Put.
     *
     * @param key   the key
     * @param value the value
     */
    public static void put(String key, Object value) {
        getLocal().put(key, value);
    }

    /**
     * Clear.
     *
     * @param key the key
     */
    public static void clear(String key) {
        getLocal().put(key, null);
    }

    /**
     * Sets request id.
     *
     * @param id the id
     */
    public static void setRequestId(final String id) {
        put(LOCAL_REQUEST_ID, id);
    }

    /**
     * Gets request id.
     *
     * @return the request id
     */
    public static String getRequestId() {
        return get(LOCAL_REQUEST_ID, String.class);
    }

}


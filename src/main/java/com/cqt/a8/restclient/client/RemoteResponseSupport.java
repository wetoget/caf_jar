package com.cqt.a8.restclient.client;


import com.cqt.a8.restclient.exception.entity.ResponseErrorMessage;
import org.springframework.http.ResponseEntity;

/**
 * The interface Remote response support.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public interface RemoteResponseSupport {
    /**
     * Gets remote response entity.
     *
     * @return the remote response entity
     */
    ResponseEntity<ResponseErrorMessage> getRemoteResponseEntity();
}


package com.cqt.a8.restclient.exception.entity;

import com.cqt.a8.restclient.i18n.I18NProvider;
import org.springframework.util.StringUtils;

import java.io.Serializable;

/**
 * 错误信息封装类.
 *
 * @program: caf
 * @description:
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class ErrorMessage implements Serializable {
    private static final long serialVersionUID = -5401402542472113075L;

    private String code;
    private String message;
    private String detail;
    private ResponseErrorMessage cause;


    /**
     * Instantiates a new Error message.
     */
    public ErrorMessage() {
    }

    /**
     * Instantiates a new Error message.
     *
     * @param code the code
     */
    public ErrorMessage(String code) {
        this(code, null, null);
    }

    /**
     * Instantiates a new Error message.
     *
     * @param code    the code
     * @param message the message
     */
    public ErrorMessage(String code, String message) {
        this(code, message, null);
    }

    /**
     * Instantiates a new Error message.
     *
     * @param code    the code
     * @param message the message
     * @param detail  the detail
     */
    public ErrorMessage(String code, String message, String detail) {
        this(code, message, detail, null);
    }

    /**
     * Instantiates a new Error message.
     *
     * @param code    the code
     * @param message the message
     * @param detail  the detail
     * @param cause   the cause
     */
    public ErrorMessage(String code, String message, String detail, ResponseErrorMessage cause) {
        this.message = StringUtils.isEmpty(message)?"null($WAF)":message;
        this.code    = code;
        this.detail  = detail;
        this.cause   = cause;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param code the code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets message.
     *
     * @param message the message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Get localized message string.
     *
     * @return the string
     */
    public String getLocalizedMessage(){
        return I18NProvider.getString(message);
    }

    /**
     * Gets detail.
     *
     * @return the detail
     */
    public String getDetail() {
        return detail;
    }

    /**
     * Sets detail.
     *
     * @param detail the detail
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * 获得远程服务返回的错误信息
     *
     * @return 远程该服务返回的错误信息 cause
     * @since 0.9.6
     */
    public ResponseErrorMessage getCause() {
        return cause;
    }

    /**
     * 设置远程服务返回的错误信息
     *
     * @param cause 远程服务返回的错误信息
     * @since 0.9.6
     */
    public void setCause(ResponseErrorMessage cause) {
        this.cause = cause;
    }

    /**
     * Convert response error message.
     *
     * @param throwable the throwable
     * @return the response error message
     */
    public ResponseErrorMessage convert(Throwable throwable) {
        ResponseErrorMessage responseErrorMessage = new ResponseErrorMessage(throwable);
        responseErrorMessage.setMessage(getLocalizedMessage());
        responseErrorMessage.setCode(code);
        responseErrorMessage.setDetail(detail);
        responseErrorMessage.setCause(cause);
        return responseErrorMessage;
    }
}

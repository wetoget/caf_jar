package com.cqt.a8.restclient.exception.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

/**
 * The type Response error message.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class ResponseErrorMessage extends ErrorMessage implements Cloneable {

    private String hostId;
    private String requestId;
    private Date serverTime;
    private Throwable throwable;

    /**
     * Instantiates a new Response error message.
     */
    public ResponseErrorMessage() {
    }

    /**
     * Instantiates a new Response error message.
     *
     * @param throwable the throwable
     */
    public ResponseErrorMessage(Throwable throwable) {
        this.throwable = throwable;
    }

    /**
     * Gets host id.
     *
     * @return the host id
     */
    public String getHostId() {
        return hostId;
    }

    /**
     * Sets host id.
     *
     * @param hostId the host id
     */
    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    /**
     * Gets request id.
     *
     * @return the request id
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Sets request id.
     *
     * @param requestId the request id
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Gets server time.
     *
     * @return the server time
     */
    public Date getServerTime() {
        return serverTime;
    }

    /**
     * Sets server time.
     *
     * @param serverTime the server time
     */
    public void setServerTime(Date serverTime) {
        this.serverTime = serverTime;
    }

    /**
     * Gets throwable.
     *
     * @return the throwable
     */
    @JsonIgnore
    public Throwable getThrowable() {
        return throwable;
    }

    /**
     * Sets throwable.
     *
     * @param throwable the throwable
     */
    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("<");
        builder.append("code:")
                .append(getCode())
                .append(", message:")
                .append(getMessage())
                .append(", host_id:")
                .append(hostId)
                .append(", server_time:")
                .append(serverTime)
                .append(", request_id:")
                .append(requestId)
                .append(", detail:")
                .append(getDetail())
                .append(">");
        return builder.toString();
    }

    public ResponseErrorMessage clone() {
        try {
            return (ResponseErrorMessage) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

}

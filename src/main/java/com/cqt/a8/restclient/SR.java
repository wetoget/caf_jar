package com.cqt.a8.restclient;

import com.cqt.a8.restclient.i18n.I18NProvider;

import java.util.Locale;

/**
 * The type Sr.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class SR {
    /**
     * Gets string.
     *
     * @param key the key
     * @return the string
     */
    public static String getString(String key) {
        return I18NProvider.getString(key);
    }

    /**
     * Gets string.
     *
     * @param locale the locale
     * @param key    the key
     * @return the string
     */
    public static String getString(Locale locale, String key) {
        return I18NProvider.getString(locale, key);
    }

    /**
     * Format string.
     *
     * @param key  the key
     * @param args the args
     * @return the string
     */
    public static String format(String key, Object... args) {
        return String.format(I18NProvider.getString(key), args);
    }

    /**
     * Format string.
     *
     * @param locale the locale
     * @param key    the key
     * @param args   the args
     * @return the string
     */
    public static String format(Locale locale, String key, Object... args) {
        return String.format(I18NProvider.getString(locale, key), args);
    }
}

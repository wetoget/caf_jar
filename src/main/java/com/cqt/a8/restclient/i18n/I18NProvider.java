package com.cqt.a8.restclient.i18n;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.util.Assert;
import sun.util.ResourceBundleEnumeration;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

/**
 * The type I18N provider.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class I18NProvider {

    private static I18NProvider provider;

    static {
        provider = new I18NProvider();
    }

    /**
     * Sets provider.
     *
     * @param provider the provider
     */
    public static void setProvider(I18NProvider provider) {
        I18NProvider.provider = provider;
    }

    /**
     * Gets provider.
     *
     * @return the provider
     */
    public static I18NProvider getProvider() {
        return provider;
    }

    /**
     * Gets string.
     *
     * @param name the name
     * @return the string
     */
    public static String getString(String name) {
        Assert.hasText(name);
        if (name.startsWith("resource:")) {
            name = name.substring(9).trim();
        }
        ResourceBundle resourceBundle = provider.getResourceBundle();
        if (resourceBundle.containsKey(name)) {
            return resourceBundle.getString(name);
        }
        return name;
    }

    /**
     * Gets string.
     *
     * @param locale the locale
     * @param name   the name
     * @return the string
     */
    public static String getString(Locale locale, String name) {
        Assert.hasText(name);
        Assert.notNull(locale);
        
        if (name.startsWith("resource:")) {
            name = name.substring(9).trim();
        }
        ResourceBundle resourceBundle = provider.getResourceBundle(locale);
        if (resourceBundle.containsKey(name)) {
            return resourceBundle.getString(name);
        }
        return name;
    }

    /**
     * Contains key boolean.
     *
     * @param name the name
     * @return the boolean
     */
    public static boolean containsKey(String name) {
        return provider.getResourceBundle().containsKey(name);
    }

    /**
     * Contains key boolean.
     *
     * @param locale the locale
     * @param name   the name
     * @return the boolean
     */
    public static boolean containsKey(Locale locale, String name) {
        return provider.getResourceBundle(locale).containsKey(name);
    }

    /**
     * Gets resource bundle.
     *
     * @return the resource bundle
     */
    public ResourceBundle getResourceBundle() {
        Locale locale = getDefaultLocale();
        MultiControl multiControl = new MultiControl();
        ResourceBundle resourceBundle = ResourceBundle.getBundle("caf_resource", locale, multiControl);
        return resourceBundle;
    }

    /**
     * Gets resource bundle.
     *
     * @param locale the locale
     * @return the resource bundle
     */
    public ResourceBundle getResourceBundle(Locale locale) {
        return ResourceBundle.getBundle("caf_resource", locale, new MultiControl());
    }

    /**
     * Gets default locale.
     *
     * @return the default locale
     */
    protected Locale getDefaultLocale() {
        return LocaleContextHolder.getLocale();
    }

    private static class MultiControl extends ResourceBundle.Control {
        public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
                throws IllegalAccessException, InstantiationException, IOException {
            String bundleName = toBundleName(baseName, locale);
            String resourceName = toResourceName(bundleName, "properties");

            List<URL> resources = Collections.list(loader.getResources(resourceName));
            Collections.reverse(resources);
            return new MultiResourcePropertyResourceBundle(resources);
        }
    }

    private static class MultiResourcePropertyResourceBundle extends ResourceBundle {
        private final HashMap lookup;

        /**
         * Instantiates a new Multi resource property resource bundle.
         *
         * @param urls the urls
         * @throws IOException the io exception
         */
        public MultiResourcePropertyResourceBundle(List<URL> urls) throws IOException {
            lookup = new HashMap();
            for (URL url : urls) {
                URLConnection urlc = url.openConnection();
                InputStream is = urlc.getInputStream();
                try {
                    Properties temp = new Properties();
                    temp.load(is);
                    lookup.putAll(temp);
                } finally {
                    is.close();
                }
            }
        }

        @Override
        protected Object handleGetObject(String key) {
            if (key == null) {
                throw new NullPointerException();
            }
            return lookup.get(key);
        }

        @Override
        public Enumeration<String> getKeys() {
            ResourceBundle parent = this.parent;
            return new ResourceBundleEnumeration(lookup.keySet(),
                    (parent != null) ? parent.getKeys() : null);
        }

        @Override
        protected Set<String> handleKeySet() {
            return lookup.keySet();
        }
    }

}

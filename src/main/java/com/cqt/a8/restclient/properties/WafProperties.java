package com.cqt.a8.restclient.properties;

import com.cqt.a8.restclient.support.ConfKey;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

/**
 * 提供 CAF 使用到的配置信息的管理。可以使用 waf.properties 配置文件进行配置，
 * 或则使用 {@link com.cqt.a8.restclient.properties.WafProperties#setProperty} 进行代码配置.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class WafProperties {
    private WafProperties(){
    }

    //private static final Logger logger = LoggerFactory.getLogger(WafProperties.class);
    private static final Logger logger = LogManager.getLogger(WafProperties.class);
    private static Properties properties;
    private static Properties defaultProperties;

    static {
        defaultProperties = new Properties();

        load();
    }

    /**
     * Load.
     */
    public static void load(){
        try {
            properties = new Properties(defaultProperties);
            InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("waf.properties");
            if (stream != null) {
                properties.load(stream);
                stream.close();
            }
        } catch (Exception ex) {
            logger.error("Read waf.properties error.", ex);
        }
    }

    /**
     * Save boolean.
     *
     * @return the boolean
     */
    public static boolean save() {
        boolean res = false;

        try {
            URL url = Thread.currentThread().getContextClassLoader().getResource("waf.properties");
            if (url != null) {
                properties.store(new FileOutputStream(new File(url.toURI())), null);
                res = true;
            }
        }
        catch (Exception ex){
            logger.error("Write waf.properties error.", ex);
        }

        return res;
    }

    /**
     * Gets properties.
     *
     * @return the properties
     */
    public static Properties getProperties() {
        return properties;
    }

    /**
     * Gets default properties.
     *
     * @return the default properties
     */
    public static Properties getDefaultProperties() {
        return defaultProperties;
    }

    /**
     * Gets property.
     *
     * @param key the key
     * @return the property
     */
    public static String getProperty(String key) {
        return properties.getProperty(key);
    }

    /**
     * Gets property.
     *
     * @param key          the key
     * @param defaultValue the default value
     * @return the property
     */
    public static String getProperty(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    /**
     * Get property string.
     *
     * @param key the key
     * @return the string
     */
    public static String getProperty(ConfKey key){
        return properties.getProperty(key.getKey(), key.getDefaultValue());
    }

    /**
     * Sets property.
     *
     * @param key   the key
     * @param value the value
     */
    public static void setProperty(String key, String value) {
        properties.setProperty(key, value);
    }

    /**
     * Gets property for integer.
     *
     * @param key the key
     * @return the property for integer
     */
    public static int getPropertyForInteger(String key) {
        String value = getProperty(key);
        try {
            return Integer.parseInt(value);
        } catch (Exception ex) {
            logger.error("转换 \"" + value + "\" 为 int 过程发生错误，引发的 properties 属性为 " + key, ex);
            throw new IllegalArgumentException("转换 \"" + value + "\" 为 int 过程发生错误，引发的 properties 属性为 " + key);
        }
    }

    /**
     * Get property for integer int.
     *
     * @param key the key
     * @return the int
     */
    public static int getPropertyForInteger(ConfKey key){
        return getPropertyForInteger(key.getKey(), key.getDefaultValue());
    }

    /**
     * Gets property for integer.
     *
     * @param key          the key
     * @param defaultValue the default value
     * @return the property for integer
     */
    public static int getPropertyForInteger(String key, String defaultValue) {
        String value = getProperty(key, defaultValue);
        try {
            return Integer.parseInt(value);
        } catch (Exception ex) {
            logger.error("转换 \"" + value + "\" 为 int 过程发生错误，引发的 properties 属性为 " + key, ex);
            throw new IllegalArgumentException("转换 \"" + value + "\" 为 int 过程发生错误，引发的 properties 属性为 " + key);
        }
    }

    /**
     * Get property for boolean boolean.
     *
     * @param key the key
     * @return the boolean
     */
    public static boolean getPropertyForBoolean(ConfKey key){
        return getPropertyForBoolean( key.getKey(), key.getDefaultValue() );
    }

    /**
     * Gets property for boolean.
     *
     * @param key          the key
     * @param defaultValue the default value
     * @return the property for boolean
     */
    public static boolean getPropertyForBoolean(String key, String defaultValue) {
        return Boolean.parseBoolean( getProperty(key, defaultValue) );
    }
}

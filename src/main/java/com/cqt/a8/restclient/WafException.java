package com.cqt.a8.restclient;

import com.cqt.a8.restclient.exception.entity.ErrorMessage;
import com.cqt.a8.restclient.i18n.I18NProvider;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * 抛出一个符合 Caf error 信息规范的异常。包含有错误信息 errorMessage, http status, response headers。.
 *
 * @program: caf
 * @description:
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class WafException extends RuntimeException {
    private ResponseEntity<ErrorMessage> responseEntity;

    /**
     * Instantiates a new Waf exception.
     *
     * @param responseEntity the response entity
     * @param cause          the cause
     */
    public WafException(ResponseEntity<ErrorMessage> responseEntity, Throwable cause) {
        super(responseEntity.getBody().getMessage(), cause);
        this.responseEntity = responseEntity;
    }

    /**
     * Instantiates a new Waf exception.
     *
     * @param responseEntity the response entity
     */
    public WafException(ResponseEntity<ErrorMessage> responseEntity) {
        this(responseEntity, null);
    }

    /**
     * Instantiates a new Waf exception.
     *
     * @param errorMessage the error message
     * @param status       the status
     * @param cause        the cause
     */
    public WafException(ErrorMessage errorMessage, HttpStatus status, Throwable cause) {
        this(new ResponseEntity<>(errorMessage, status), cause);
    }

    /**
     * Instantiates a new Waf exception.
     *
     * @param errorMessage the error message
     * @param status       the status
     */
    public WafException(ErrorMessage errorMessage, HttpStatus status) {
        this(new ResponseEntity<>(errorMessage, status));
    }

    /**
     * Instantiates a new Waf exception.
     *
     * @param code    the code
     * @param message the message
     * @param detail  the detail
     * @param status  the status
     * @param cause   the cause
     */
    public WafException(String code, String message, String detail, HttpStatus status, Throwable cause) {
        this(new ErrorMessage(code, message, detail), status, cause);
    }

    /**
     * Instantiates a new Waf exception.
     *
     * @param code    the code
     * @param message the message
     * @param detail  the detail
     * @param status  the status
     */
    public WafException(String code, String message, String detail, HttpStatus status) {
        this(new ErrorMessage(code, message, detail), status, null);
    }

    /**
     * Instantiates a new Waf exception.
     *
     * @param code    the code
     * @param message the message
     * @param status  the status
     * @param cause   the cause
     */
    public WafException(String code, String message, HttpStatus status, Throwable cause) {
        this(new ErrorMessage(code, message), status, cause);
    }

    /**
     * Instantiates a new Waf exception.
     *
     * @param code    the code
     * @param message the message
     * @param status  the status
     */
    public WafException(String code, String message, HttpStatus status) {
        this(code, message, status, null);
    }

    /**
     * Instantiates a new Waf exception.
     *
     * @param code    the code
     * @param message the message
     * @param cause   the cause
     */
    public WafException(String code, String message, Throwable cause) {
        this(new ErrorMessage(code, message), HttpStatus.INTERNAL_SERVER_ERROR, cause);
    }

    /**
     * Instantiates a new Waf exception.
     *
     * @param code    the code
     * @param message the message
     */
    public WafException(String code, String message) {
        this(code, message, (Throwable) null);
    }

    /**
     * Gets response entity.
     *
     * @return the response entity
     */
    public ResponseEntity<ErrorMessage> getResponseEntity() {
        return responseEntity;
    }

    /**
     * Gets error.
     *
     * @return the error
     */
    public ErrorMessage getError() {
        return responseEntity.getBody();
    }

	@Override
    public String getLocalizedMessage() {
        return I18NProvider.getString(getMessage());
    }
}

package com.cqt.a8.rest.filter;

import com.cqt.a8.rest.utils.ExceptUtil;
import com.cqt.a8.restclient.util.WafJsonMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.NullNode;
import org.apache.commons.collections4.IteratorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

/**
 * ie8/ie9跨域支持过滤器.
 *
 * @program: caf
 * @description:
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class CafHttpMethodOverrideFilter extends OncePerRequestFilter {
    /**
     * The constant PROXY_PARAM.
     */
    public static final String PROXY_PARAM = "$proxy";
    /**
     * The constant PROXY_PARAM_ENCODE.
     */
    public static final String PROXY_PARAM_ENCODE = "%24proxy";
    /**
     * The constant METHOD_PARAM.
     */
    public static final String METHOD_PARAM = "$method";
    /**
     * The constant BODY_PARAM.
     */
    public static final String BODY_PARAM = "$body";
    /**
     * The constant HEADERS_PARAM.
     */
    public static final String HEADERS_PARAM = "$headers";
    /**
     * The constant STATUS.
     */
    public static final String STATUS = "$status";
    /**
     * The constant STATUS_TEXT.
     */
    public static final String STATUS_TEXT = "$status_text";

    private static final String defaultCharset = "UTF-8";

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        log.debug("Http override filter start");
        String proxyValue = request.getParameter(PROXY_PARAM);
        if ("POST".equals(request.getMethod()) && "body".equals(proxyValue)) {

            ObjectMapper mapper = WafJsonMapper.getMapper();
            JsonNode proxyNode = mapper.readTree(request.getInputStream());
            JsonNode methodNode = proxyNode.get(METHOD_PARAM);
            ExceptUtil.checkNotNull(methodNode, "属性 " + METHOD_PARAM + " 不能为空");

            JsonNode headersNode = proxyNode.get(HEADERS_PARAM);
            if (headersNode == null) {
                headersNode = NullNode.getInstance();
            }
            JsonNode bodyNode = proxyNode.get(BODY_PARAM);
            String bodyStr = null;
            if (bodyNode!=null) {
            	bodyStr = bodyNode.asText();
            	if (StringUtils.isEmpty(bodyStr)) {
            		bodyStr = bodyNode.toString();
				}
			}
            
            HttpServletRequest httpRequestWrapper = new HttpMethodRequestWrapper(request, methodNode.asText(), headersNode, bodyStr);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            HttpServletResponseWrapper httpResponseWrapper = new HttpServletResponseWrapper(response, os);

            filterChain.doFilter(httpRequestWrapper, httpResponseWrapper);
            //刷新response的输出流, 在某些情况下, 如果不刷新的话, 会导致部分信息还在缓存中, 而没有写入os中
            handleResponse(os, httpResponseWrapper, response, headersNode);

        } else {
            filterChain.doFilter(request, response);
        }
		log.debug("Http override filter end");
    }

    private void handleResponse(ByteArrayOutputStream os, HttpServletResponseWrapper httpResponseWrapper, HttpServletResponse response, JsonNode headersObject) throws IOException {
		Map<String, Object> map = new HashMap<>();
		map.put(HEADERS_PARAM, headersObject);

		map.put(STATUS, httpResponseWrapper.getStatus());
		map.put(STATUS_TEXT, HttpStatus.valueOf(httpResponseWrapper.getStatus()).getReasonPhrase());

		String body = new String(os.toByteArray(), defaultCharset);
		if (!StringUtils.isEmpty(body)) {
			map.put(BODY_PARAM, body);
		}

        if ( response.getContentType() == null ){
            response.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        }
		
		response.setCharacterEncoding(defaultCharset);
		PrintWriter out   = response.getWriter();
        final String json = WafJsonMapper.toJson(map);
        response.setContentLength(json.getBytes(defaultCharset).length);
		out.println(json);
        out.flush();
    }

    private static class HttpMethodRequestWrapper extends HttpServletRequestWrapper {

        private final HttpServletRequest request;
        private final String method;
        private final JsonNode headersNode;
        private String body;

        /**
         * Instantiates a new Http method request wrapper.
         *
         * @param request     the request
         * @param method      the method
         * @param headersNode the headers node
         * @param bodyNode    the body node
         */
        public HttpMethodRequestWrapper(HttpServletRequest request, String method, JsonNode headersNode, String bodyNode) {
            super(request);
            this.request = request;
            this.method = method.toUpperCase(Locale.getDefault());
            if (headersNode == null) {
                headersNode = NullNode.getInstance();
            }
            this.headersNode = headersNode;
            if (bodyNode != null) {
                this.body = bodyNode;
            }
        }

        private String tryGetString(JsonNode node, String name) {
            JsonNode jsonNode = node.get(name);
            if (jsonNode != null) {
                return jsonNode.asText();
            }
            return null;
        }

        private long tryGetLong(JsonNode node, String name) {
            JsonNode jsonNode = node.get(name);
            if (jsonNode != null) {
                return jsonNode.asLong();
            }
            return 0;
        }

        private int tryGetInt(JsonNode node, String name) {
            JsonNode jsonNode = node.get(name);
            if (jsonNode != null) {
                return jsonNode.asInt();
            }
            return 0;
        }

		@Override
		public String getQueryString() {
			StringBuilder reqQueryStr = new StringBuilder();
			String queryStr = request.getQueryString();
			// 判断请求参数是否为空
			if (!StringUtils.isEmpty(queryStr)) {
				if (queryStr.contains("&")) {
					String[] paramString = queryStr.split("&");
					for (String string : paramString) {
						if (string.contains(PROXY_PARAM) || string.contains(PROXY_PARAM_ENCODE)) {
							continue;
						}
						reqQueryStr.append(string).append("&");// 参数
					}
					queryStr = reqQueryStr.toString();
					if (queryStr.lastIndexOf('&') > -1) {
						queryStr = queryStr.substring(0, queryStr.length() - 1);
					}
				} else if (!queryStr.contains(PROXY_PARAM) || !queryStr.contains(PROXY_PARAM_ENCODE)) {
					queryStr = "";
				}
			}
			return queryStr;
		}

		@Override
        public long getDateHeader(String name) {
            return tryGetLong(headersNode, name);
        }

        @Override
        public String getContentType() {
            return tryGetString(headersNode, HttpHeaders.CONTENT_TYPE);
        }

        @Override
        public String getHeader(String name) {
            String value = tryGetString(headersNode, name);
            if ( null==value && name.equals(HttpHeaders.HOST) ){
                value = request.getHeader(HttpHeaders.HOST);
            }
            return value;
        }

        @Override
        public Enumeration<String> getHeaders(String name) {
            String value = getHeader(name);
            if (value != null) {
                return Collections.enumeration(Collections.singletonList(value));
            }
            return Collections.emptyEnumeration();
        }

        @Override
        public Enumeration<String> getHeaderNames() {
            return IteratorUtils.asEnumeration(headersNode.fieldNames());
        }

        @Override
        public String getAuthType() {
            return super.getAuthType();
        }

        @Override
        public String getCharacterEncoding() {
            return super.getCharacterEncoding();
        }

        @Override
        public int getContentLength() {
            return super.getContentLength();
        }

        @Override
        public int getIntHeader(String name) {
            return tryGetInt(headersNode, name);
        }

        @Override
        public String getMethod() {
            return this.method;
        }

        @Override
        public ServletInputStream getInputStream() throws IOException {
            final ByteArrayInputStream byteArrayInputStream;
            if (body == null) {
                byteArrayInputStream = new ByteArrayInputStream(new byte[0]);
            } else {
                byteArrayInputStream = new ByteArrayInputStream(body.getBytes(defaultCharset));
            }
            return new ServletInputStream() {
                @Override
                public int read() throws IOException {
                    return byteArrayInputStream.read();
                }

                @Override
                public boolean isFinished() {
                    return false;
                }

                @Override
                public boolean isReady() {
                    return false;
                }

                @Override
                public void setReadListener(ReadListener readListener) {
                }
            };
        }

        @Override
        public BufferedReader getReader() throws IOException {
            return new BufferedReader(new InputStreamReader(
                    this.getInputStream()));
        }
    }

    private static class HttpServletResponseWrapper extends
            javax.servlet.http.HttpServletResponseWrapper implements
            Serializable {

        private static final long serialVersionUID = -6823255025479924073L;

        /**
         * Instantiates a new Http servlet response wrapper.
         *
         * @param response     the response
         * @param outputStream the output stream
         */
        public HttpServletResponseWrapper(HttpServletResponse response,
                                          OutputStream outputStream) {
            super(response);
            statusCode = 200;
            this.servletOutputStream = new ServletOutputStreamWrapper(outputStream);
        }

        @Override
        public ServletOutputStream getOutputStream() {
            return servletOutputStream;
        }

        @Override
        public void setStatus(int code) {
            statusCode = code;
            super.setStatus(200);
        }

        @Override
        public void sendError(int i, String string) throws IOException {
            statusCode = i;
            super.sendError(i, string);
        }

        @Override
        public void sendError(int i) throws IOException {
            statusCode = i;
            super.sendError(i);
        }

        @Override
        public void sendRedirect(String string) throws IOException {
            statusCode = 302;
            super.sendRedirect(string);
        }

        @Override
        public void setStatus(int code, String msg) {
            statusCode = code;
            super.setStatus(code);
        }

        @Override
        public int getStatus() {
            return statusCode;
        }

        @Override
        public void setContentLength(int length) {
            contentLength = length;
            super.setContentLength(length);
        }

        /**
         * Gets content length.
         *
         * @return the content length
         */
        public int getContentLength() {
            return contentLength;
        }

        @Override
        public void setContentType(String type) {
            contentType = type;
            super.setContentType(type);
        }

        @Override
        public String getContentType() {
            return contentType;
        }

        @Override
        public PrintWriter getWriter() throws IOException {
            if (writer == null) {
                writer = new PrintWriter(new OutputStreamWriter(
                        servletOutputStream, getCharacterEncoding()), true);
            }
            return writer;
        }

        @Override
        public void addHeader(String name, String value) {
            String header[] = {name, value};
            headers.add(header);
            super.addHeader(name, value);
        }

        @Override
        public void setHeader(String name, String value) {
            addHeader(name, value);
        }

        /**
         * Gets headers.
         *
         * @return the headers
         */
        public Collection<String[]> getHeaders() {
            return headers;
        }

        @Override
        public void addCookie(Cookie cookie) {
            cookies.add(cookie);
            super.addCookie(cookie);
        }

        /**
         * Gets cookies.
         *
         * @return the cookies
         */
        public Collection<Cookie> getCookies() {
            return cookies;
        }

        @Override
        public void flushBuffer() throws IOException {
            flush();
            super.flushBuffer();
        }

        @Override
        public void reset() {
            super.reset();
            cookies.clear();
            headers.clear();
            statusCode = 200;
            contentType = null;
            contentLength = 0;
        }

        @Override
        public void resetBuffer() {
            super.resetBuffer();
        }

        /**
         * Flush.
         *
         * @throws IOException the io exception
         */
        public void flush() throws IOException {
            if (writer != null) {
                writer.flush();
            }
            servletOutputStream.flush();
        }

        @Override
        public String encodeRedirectUrl(String s) {
            return super.encodeRedirectURL(s);
        }

        @Override
        public String encodeUrl(String s) {
            return super.encodeURL(s);
        }

        private int statusCode;
        private int contentLength;
        private String contentType;
        private final List<String[]> headers = new ArrayList<>();
        private final List<Cookie> cookies = new ArrayList<>();
        private final ServletOutputStream servletOutputStream;
        private PrintWriter writer;
    }

    private static class ServletOutputStreamWrapper extends ServletOutputStream {

        /**
         * Instantiates a new Servlet output stream wrapper.
         *
         * @param stream the stream
         */
        public ServletOutputStreamWrapper(OutputStream stream) {
            this.stream = stream;
        }

        @Override
        public void write(int b) throws IOException {
            stream.write(b);
        }

        @Override
        public void write(byte b[]) throws IOException {
            stream.write(b);
        }

        @Override
        public void write(byte b[], int off, int len) throws IOException {
            stream.write(b, off, len);
        }

        private final OutputStream stream;

        @Override
        public boolean isReady() {
            return false;
        }

        @Override
        public void setWriteListener(WriteListener writeListener) {
        }
    }


}

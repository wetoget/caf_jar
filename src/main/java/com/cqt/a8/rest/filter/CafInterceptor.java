package com.cqt.a8.rest.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Caf 主要的spring拦截器.
 *
 * @program: caf
 * @description:
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
@Slf4j
public class CafInterceptor extends HandlerInterceptorAdapter {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){
//        String bestMatching = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
//        if (null == bestMatching) {
//            return true;
//        }
//        Integer i=1/0;
//        //找不到映射接口的情况，直接返回错误异常
//        if(handler instanceof DefaultServletHttpRequestHandler){
//            //ServletUtil.createNotFoundErrorResponse(response);
//            return false;
//        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex){

    }
}


package com.cqt.a8.rest.filter;

import com.cqt.a8.rest.exceptions.DefaultWafRestErrorResolver;
import com.cqt.a8.rest.exceptions.WafErrorResolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Exception filter.
 *
 * @program: caf
 * @description:
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class ExceptionFilter extends OncePerRequestFilter implements ApplicationContextAware {

    private static final Logger logger = LogManager.getLogger(ExceptionFilter.class);
    private List<WafErrorResolver> wafErrorResolvers;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        wafErrorResolvers = new ArrayList<>();
        WafErrorResolver wafErrorResolver = (WafErrorResolver)applicationContext.getBean("wafErrorResolver");
        if (wafErrorResolver!=null) {
            wafErrorResolvers.add(wafErrorResolver);
        }

        DefaultWafRestErrorResolver defaultWafRestErrorResolver = (DefaultWafRestErrorResolver)applicationContext.getBean("defaultWafRestErrorResolver");
        if (defaultWafRestErrorResolver!=null) {
            wafErrorResolvers.add(defaultWafRestErrorResolver);
        }
    }

    /**
     * Sets waf error resolvers.
     *
     * @param wafErrorResolvers the waf error resolvers
     */
    public void setWafErrorResolvers(List<WafErrorResolver> wafErrorResolvers) {
        this.wafErrorResolvers = wafErrorResolvers;
    }

    /**
     * Get waf error resolvers list.
     *
     * @return the list
     */
    protected List<WafErrorResolver> getWafErrorResolvers(){
        return wafErrorResolvers;
    }

    /**
     * Same contract as for {@code doFilter}, but guaranteed to be
     * just invoked once per request within a single request thread.
     * See {@link #shouldNotFilterAsyncDispatch()} for details.
     *
     * <p>Provides HttpServletRequest and HttpServletResponse arguments instead of the
     * default ServletRequest and ServletResponse ones.
     *
     * @param request
     * @param response
     * @param filterChain
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		logger.debug("Exception filter start");
        try {
            filterChain.doFilter(request, response);
        } catch (Exception ex) {
        	if (ex.getCause() != null && ex.getCause() instanceof Exception){
        		ex = (Exception) ex.getCause();
            }

            for (WafErrorResolver wafErrorResolver : wafErrorResolvers) {
                try {
                    if (wafErrorResolver.process(ex, request, response)) {
                        break;
                    }
                } catch (Exception e) {
                    ex = e;
                }
            }
        }

		logger.debug("Exception filter end");
    }
}

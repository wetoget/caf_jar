package com.cqt.a8.rest.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wangmingkui
 * @ClassName: CafCorsFilter
 * @Description: 跨域资源共享过滤器
 */
public class CafCorsFilter extends OncePerRequestFilter {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        log.debug("CAF CorsFilter doFilter start");

        // 设定CORS的初始化参数
        // cors.allowed.origins *:Any origin is allowed to access the resource
        response.addHeader("Access-Control-Allow-Origin", "*");
        // cors.allowed.methods Access-Control-Allow-Methods: A comma separated
        // list of HTTP methods that can be used to access the resource
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE, PATCH");
        // cors.allowed.headers Access-Control-Allow-Headers: A comma separated
        // list of request headers that can be used when making an actual
        // request
        response.addHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization, Cache-control");
        // cors.preflight.maxage Access-Control-Max-Age The amount of seconds,
        // browser is allowed to cache the result of the pre-flight request
        response.addHeader("Access-Control-Max-Age", "1800");

        filterChain.doFilter(request, response);
		log.debug("CAF CorsFilter doFilter end");
    }
}

package com.cqt.a8.rest.filter;

import com.cqt.a8.restclient.support.CafContext;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 清理上下文.
 *
 * @program: caf
 * @description:
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class CafContextFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        try {
            filterChain.doFilter(request, response);
        }finally {
            CafContext.getLocal().clear();
        }

    }
}


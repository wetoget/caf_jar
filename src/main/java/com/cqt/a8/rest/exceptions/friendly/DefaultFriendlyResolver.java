package com.cqt.a8.rest.exceptions.friendly;

import com.cqt.a8.restclient.exception.entity.ResponseErrorMessage;
import com.cqt.a8.restclient.i18n.I18NProvider;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Default friendly resolver.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018-04-25 10:56
 */
@Component
public class DefaultFriendlyResolver extends FriendlyExceptionMessageConverter {
    @Override
    public boolean convert(ResponseEntity<ResponseErrorMessage> responseEntity, HttpServletRequest request) {
        int statusCode = responseEntity.getStatusCode().value();
        String name  = "caf.er.status.code." + statusCode;
        String value = I18NProvider.getString(request.getLocale(), name);

        if (value.equals(name)) {
            return false;
        } else {
            updateErrorMessage(responseEntity, value);
            return true;
        }
    }
}

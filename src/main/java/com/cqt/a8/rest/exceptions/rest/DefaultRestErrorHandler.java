package com.cqt.a8.rest.exceptions.rest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Default rest error handler.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class DefaultRestErrorHandler extends AbstractRestErrorHandler {

    private HttpStatus httpStatus;
    private String code;
    private HttpHeaders httpHeaders;

    /**
     * Instantiates a new Default rest error handler.
     *
     * @param httpStatus the http status
     */
    public DefaultRestErrorHandler(HttpStatus httpStatus) {
        this(httpStatus, "CAF/" + httpStatus.getReasonPhrase().toUpperCase().replace(" ", "_"));
    }

    /**
     * Instantiates a new Default rest error handler.
     *
     * @param httpStatus the http status
     * @param code       the code
     */
    public DefaultRestErrorHandler(HttpStatus httpStatus, String code) {
        this(httpStatus, code, null);
    }

    /**
     * Instantiates a new Default rest error handler.
     *
     * @param httpStatus  the http status
     * @param code        the code
     * @param httpHeaders the http headers
     */
    public DefaultRestErrorHandler(HttpStatus httpStatus, String code, HttpHeaders httpHeaders) {
        this.httpStatus = httpStatus;
        this.code = code;
        this.httpHeaders = httpHeaders;
    }

    @Override
    protected HttpStatus getHttpStatus(Throwable throwable, HttpServletRequest request) {
        return httpStatus;
    }

    @Override
    protected String getCode(Throwable throwable, HttpServletRequest request) {
        return code;
    }

    @Override
    protected HttpHeaders getHttpHandlers(Throwable throwable, HttpServletRequest request) {
        return httpHeaders;
    }
}

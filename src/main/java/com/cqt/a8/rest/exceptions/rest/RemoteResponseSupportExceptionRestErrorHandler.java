package com.cqt.a8.rest.exceptions.rest;


import com.cqt.a8.rest.support.SConfKeys;
import com.cqt.a8.restclient.client.RemoteResponseSupport;
import com.cqt.a8.restclient.exception.entity.ResponseErrorMessage;
import com.cqt.a8.restclient.properties.WafProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Remote response support exception rest error handler.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
class RemoteResponseSupportExceptionRestErrorHandler extends AbstractRestErrorHandler {
    private final HttpStatus httpStatus;
    private final boolean httpStatusProxy;

    /**
     * Instantiates a new Remote response support exception rest error handler.
     *
     * @param httpStatus the http status
     */
    public RemoteResponseSupportExceptionRestErrorHandler(HttpStatus httpStatus) {
        this.httpStatus      = httpStatus;
        this.httpStatusProxy = WafProperties.getPropertyForBoolean(SConfKeys.CAF_EXCEPTION_REMOTE_HTTPSTATUS_PROXY);
    }

    @Override
    public ResponseEntity<ResponseErrorMessage> process(Throwable throwable, HttpServletRequest request) {

        RemoteResponseSupport rrs = (RemoteResponseSupport) throwable;
        ResponseEntity<ResponseErrorMessage> remoteResponseEntity = rrs.getRemoteResponseEntity();
        ResponseErrorMessage errorMessage;
        HttpStatus status = getHttpStatus(throwable, request);
        if (remoteResponseEntity == null) {
            errorMessage = super.getBody(throwable, request);
        } else {
            ResponseErrorMessage remoteBody = remoteResponseEntity.getBody();
            errorMessage = remoteBody.clone();
            errorMessage.setDetail(appendStackTrace(remoteBody.getDetail(), throwable));
            errorMessage.setCause(remoteBody);
            updateRemoteErrorMessage(errorMessage, request);
            if ( !httpStatusProxy ){
                status = remoteResponseEntity.getStatusCode();
            }
        }
        HttpHeaders httpHandlers = getHttpHandlers(throwable, request);
        return new ResponseEntity<>(errorMessage, httpHandlers, status);
    }

    @Override
    protected String getCode(Throwable throwable, HttpServletRequest request) {
        return "CAF/" + httpStatus.getReasonPhrase().toUpperCase();
    }

    @Override
    protected HttpStatus getHttpStatus(Throwable throwable, HttpServletRequest request) {
        return httpStatus;
    }
}

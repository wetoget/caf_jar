package com.cqt.a8.rest.exceptions.rest;

import com.cqt.a8.restclient.WafException;
import com.cqt.a8.restclient.exception.entity.ErrorMessage;
import com.cqt.a8.restclient.exception.entity.ResponseErrorMessage;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;

/**
 * 默认的 {link com.cqt.a8.WafException} 的异常处理对象.
 *
 * @program: caf
 * @description:
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
class WafExceptionRestErrorHandler extends AbstractRestErrorHandler {
    @Override
    public ResponseEntity<ResponseErrorMessage> process(Throwable throwable, HttpServletRequest request) {
        Assert.isInstanceOf(WafException.class, throwable);

        ResponseEntity<ErrorMessage> responseEntity = ((WafException) throwable).getResponseEntity();
        ErrorMessage body = responseEntity.getBody();

        ResponseErrorMessage responseErrorMessage = body.convert(throwable);
        //append stack trace
        responseErrorMessage.setDetail(appendStackTrace(body.getDetail(), throwable));
        updateRemoteErrorMessage(responseErrorMessage, request);

        return new ResponseEntity<>(responseErrorMessage, responseEntity.getHeaders(), responseEntity.getStatusCode());
    }
}

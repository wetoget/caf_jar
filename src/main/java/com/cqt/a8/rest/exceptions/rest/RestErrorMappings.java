package com.cqt.a8.rest.exceptions.rest;


import com.cqt.a8.restclient.WafException;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

/**
 * The enum Rest error mappings.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public enum RestErrorMappings {
    /**
     * The Default error.
     */
// 默认错误
    DEFAULT_ERROR(Exception.class, new DefaultRestErrorHandler(HttpStatus.INTERNAL_SERVER_ERROR)),
    /**
     * The No such request handling method.
     */
// 404错误异常
    NO_SUCH_REQUEST_HANDLING_METHOD(NoSuchRequestHandlingMethodException.class, new DefaultRestErrorHandler(HttpStatus.NOT_FOUND, "CAF/URI_NOT_FOUND")),
    /**
     * The Http method not supported.
     */
// 请求方法不被支持
    HTTP_METHOD_NOT_SUPPORTED(HttpRequestMethodNotSupportedException.class, new DefaultRestErrorHandler(HttpStatus.METHOD_NOT_ALLOWED, "CAF/METHOD_NOT_ALLOWED")),
    /**
     * The Http media type not supported.
     */
// 媒体类型不被支持
    HTTP_MEDIA_TYPE_NOT_SUPPORTED(HttpMediaTypeNotSupportedException.class, new DefaultRestErrorHandler(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "CAF/UNSUPPORTED_MEDIA_TYPE")),
    /**
     * The Method argument not valid.
     */
// 方法参数验证异常
    METHOD_ARGUMENT_NOT_VALID(MethodArgumentNotValidException.class, new DefaultRestErrorHandler(HttpStatus.BAD_REQUEST, "CAF/INVALID_ARGUMENT")),
    /**
     * The Bind error.
     */
// 绑定异常
    BIND_ERROR(BindException.class, new DefaultRestErrorHandler(HttpStatus.BAD_REQUEST, "CAF/INVALID_ARGUMENT")),
    /**
     * The Conversion not support.
     */
// 转换异常
    CONVERSION_NOT_SUPPORT(ConversionNotSupportedException.class, new DefaultRestErrorHandler(HttpStatus.BAD_REQUEST, "CAF/BAD_REQUEST")),
    /**
     * The Http media type not acceptable.
     */
// 请求资源类型无法接受
    HTTP_MEDIA_TYPE_NOT_ACCEPTABLE(HttpMediaTypeNotAcceptableException.class, new DefaultRestErrorHandler(HttpStatus.NOT_ACCEPTABLE, "CAF/NOT_ACCEPTABLE")),
    /**
     * The Http message not readable.
     */
// 读取异常
    HTTP_MESSAGE_NOT_READABLE(HttpMessageNotReadableException.class, new DefaultRestErrorHandler(HttpStatus.BAD_REQUEST, "CAF/INVALID_ARGUMENT")),
    /**
     * The Http message not writable.
     */
// 写入异常
    HTTP_MESSAGE_NOT_WRITABLE(HttpMessageNotWritableException.class, new DefaultRestErrorHandler(HttpStatus.BAD_REQUEST)),
    /**
     * The Missing request parameter.
     */
// 请求参数缺失
    MISSING_REQUEST_PARAMETER(MissingServletRequestParameterException.class, new DefaultRestErrorHandler(HttpStatus.BAD_REQUEST, "CAF/REQUIRE_ARGUMENT")),
    /**
     * The Missing request part.
     */
// 请求部分缺失
    MISSING_REQUEST_PART(MissingServletRequestPartException.class, new DefaultRestErrorHandler(HttpStatus.BAD_REQUEST, "CAF/REQUIRE_ARGUMENT")),
    /**
     * The Multipart exception.
     */
//分块请求异常
    MULTIPART_EXCEPTION(MultipartException.class, new DefaultRestErrorHandler(HttpStatus.BAD_REQUEST, "CAF/MULTIPART_EXCEPTION")),
    /**
     * The Not found.
     */
// 404错误
    NOT_FOUND(NoHandlerFoundException.class, new DefaultRestErrorHandler(HttpStatus.NOT_FOUND, "CAF/URI_NOT_FOUND")),
    /**
     * The Request bind error.
     */
// 请求绑定异常
    REQUEST_BIND_ERROR(ServletRequestBindingException.class, new DefaultRestErrorHandler(HttpStatus.BAD_REQUEST, "CAF/BAD_REQUEST")),
    /**
     * The Type mismatch.
     */
// 类型匹配异常
    TYPE_MISMATCH(TypeMismatchException.class, new DefaultRestErrorHandler(HttpStatus.BAD_REQUEST, "CAF/BAD_REQUEST")),
    /**
     * The Access denied.
     */
// 访问拒绝
    ACCESS_DENIED(AccessDeniedException.class, new DefaultRestErrorHandler(HttpStatus.FORBIDDEN, "CAF/ACCESS_DENIED")),
    /**
     * The Insufficient authentication.
     */
// 认证信息不全，或者头信息格式错误
    INSUFFICIENT_AUTHENTICATION(InsufficientAuthenticationException.class, new DefaultRestErrorHandler(HttpStatus.UNAUTHORIZED, "CAF/AUTH_INVALID_TOKEN")),
    /**
     * The Authentication credential not found.
     */
// 认证凭据没找到
    AUTHENTICATION_CREDENTIAL_NOT_FOUND(AuthenticationCredentialsNotFoundException.class, new DefaultRestErrorHandler(HttpStatus.UNAUTHORIZED, "CAF/AUTH_ERROR")),

    /**
     * The Waf illegal request method.
     */
// 请求方法不支持
    WAF_ILLEGAL_REQUEST_METHOD(IllegalArgumentException.class, new IllegalRequestMethodRestErrorHandler(HttpStatus.METHOD_NOT_ALLOWED)),
    /**
     * The Waf error.
     */
// CAF错误信息
    WAF_ERROR(WafException.class, new WafExceptionRestErrorHandler());

    private Class throwableClass;
    private AbstractRestErrorHandler handler;

    RestErrorMappings(final Class throwableClass, final AbstractRestErrorHandler handler) {
        this.throwableClass = throwableClass;
        this.handler = handler;
    }

    /**
     * Gets throwable class.
     *
     * @return the throwable class
     */
    public Class getThrowableClass() {
        return throwableClass;
    }

    /**
     * Gets handler.
     *
     * @return the handler
     */
    public AbstractRestErrorHandler getHandler() {
        return handler;
    }
}

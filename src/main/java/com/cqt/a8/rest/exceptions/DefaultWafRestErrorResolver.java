package com.cqt.a8.rest.exceptions;

import com.cqt.a8.rest.config.CafConstants;
import com.cqt.a8.rest.exceptions.rest.AbstractRestErrorHandler;
import com.cqt.a8.restclient.exception.entity.ResponseErrorMessage;
import com.cqt.a8.restclient.util.WafJsonMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * The type Default waf rest error resolver.
 *
 * @program: caf
 * @description:
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class DefaultWafRestErrorResolver extends AbstractRestErrorHandler implements WafErrorResolver, Ordered {

    private static final Logger logger = LogManager.getLogger(DefaultWafRestErrorResolver.class);
	
    @Override
    public boolean process(Throwable throwable, HttpServletRequest request, HttpServletResponse response) {
    	ResponseEntity<ResponseErrorMessage> entity = process(throwable, request);

        try {
            logger.error(entity.getBody().toString(), throwable);

            response.setStatus(entity.getStatusCode().value());
            response.setContentType(CafConstants.APPLICATION_JSON_UTF8);
            PrintWriter writer = response.getWriter();
            writer.print(WafJsonMapper.toJson(entity.getBody()));
            writer.flush();
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    @Override
    public int getOrder() {
        return Integer.MAX_VALUE;
    }
}

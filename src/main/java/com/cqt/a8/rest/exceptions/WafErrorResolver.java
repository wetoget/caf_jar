package com.cqt.a8.rest.exceptions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CAF 对异常进行处理的接口.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public interface WafErrorResolver {
    /**
     * Process boolean.
     *
     * @param throwable the throwable
     * @param request   the request
     * @param response  the response
     * @return the boolean
     */
    boolean process(Throwable throwable, HttpServletRequest request, HttpServletResponse response);
}

package com.cqt.a8.rest.exceptions;

import com.cqt.a8.rest.config.CafConstants;
import com.cqt.a8.rest.exceptions.rest.AbstractRestErrorHandler;
import com.cqt.a8.restclient.exception.entity.ResponseErrorMessage;
import com.cqt.a8.restclient.util.WafJsonMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 实现对异常进行处理，以 JSON 响应异常信息.
 *
 * @program: caf
 * @description:
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
@Component
public class WafRestErrorResolver implements WafErrorResolver, Ordered {

    /**
     * The constant handlerMap.
     */
    protected static final Map<Class, AbstractRestErrorHandler> handlerMap = new HashMap<>();

    //private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final Logger logger = LogManager.getLogger(WafRestErrorResolver.class);

    /**
     * Add handler.
     *
     * @param throwableClass the throwable class
     * @param handler        the handler
     */
    public void addHandler(Class throwableClass, AbstractRestErrorHandler handler) {
        Assert.notNull(throwableClass);
        Assert.notNull(handler);

        handlerMap.put(throwableClass, handler);
    }

    /**
     * Gets handler.
     *
     * @param throwableClass the throwable class
     * @return the handler
     */
    public AbstractRestErrorHandler getHandler(Class throwableClass) {
        Assert.notNull(throwableClass);

        for (Class clazz = throwableClass; clazz != Throwable.class; clazz = clazz.getSuperclass()) {
            AbstractRestErrorHandler handler = handlerMap.get(clazz);
            if (handler != null) {
                return handler;
            }
        }
        throw new RuntimeException("无法找到异常类型为 " + throwableClass + " 的异常处理器。");
    }

    @Override
    public boolean process(Throwable throwable, HttpServletRequest request, HttpServletResponse response) {
        return this.resolver(throwable, request, response);
    }

    /**
     * Resolver boolean.
     *
     * @param throwable the throwable
     * @param request   the request
     * @param response  the response
     * @return the boolean
     */
    public boolean resolver(Throwable throwable, HttpServletRequest request, HttpServletResponse response) {
        Assert.notNull(throwable);
        Assert.notNull(request);
        Assert.notNull(response);

        ResponseEntity<ResponseErrorMessage> responseEntity = process(throwable, request);
        response.setStatus(responseEntity.getStatusCode().value());
        if (response.getStatus()>=HttpStatus.INTERNAL_SERVER_ERROR.value()) {
            //只打印服务端错误
            logger.error(responseEntity.getBody().toString(), throwable);
        }

        HttpHeaders headers = responseEntity.getHeaders();
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.toSingleValueMap().entrySet()) {
                response.setHeader(entry.getKey(), entry.getValue());
            }
        }
        try {
            Response r = new Response(responseEntity.getBody());
            response.setContentType(CafConstants.APPLICATION_JSON_UTF8);
            PrintWriter writer = response.getWriter();
            writer.print(WafJsonMapper.toJson(r));
            writer.flush();
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    /**
     * Process response entity.
     *
     * @param throwable the throwable
     * @param request   the request
     * @return the response entity
     */
    protected ResponseEntity<ResponseErrorMessage> process(Throwable throwable, HttpServletRequest request) {
        AbstractRestErrorHandler handler = getHandler(throwable.getClass());
        Assert.notNull(handler);
        return handler.process(throwable, request);
    }

    @Override
    public int getOrder() {
        return 0;
    }

    private static class Response {
        /**
         * Instantiates a new Response.
         *
         * @param errorMessage the error message
         */
        public Response(ResponseErrorMessage errorMessage) {
            hostId = errorMessage.getHostId();
            requestId = errorMessage.getRequestId();
            serverTime = errorMessage.getServerTime();
            code = errorMessage.getCode();
            message = errorMessage.getMessage();
//            if (WafContext.isDebugMode()) {
//                detail = errorMessage.getDetail();
//                cause = errorMessage.getCause();
//            }
        }

        /**
         * The Host id.
         */
        public String hostId;
        /**
         * The Request id.
         */
        public String requestId;
        /**
         * The Server time.
         */
        public Date serverTime;
        /**
         * The Code.
         */
        public String code;
        /**
         * The Message.
         */
        public String message;
        /**
         * The Detail.
         */
        public String detail;
        /**
         * The Cause.
         */
        public ResponseErrorMessage cause;
    }
}

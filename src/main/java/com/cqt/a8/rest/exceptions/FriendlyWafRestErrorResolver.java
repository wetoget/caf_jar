package com.cqt.a8.rest.exceptions;

import com.cqt.a8.rest.exceptions.friendly.FriendlyExceptionMessageConverter;
import com.cqt.a8.restclient.exception.entity.ResponseErrorMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * The type Friendly waf rest error resolver.
 *
 * @program: caf
 * @description:
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class FriendlyWafRestErrorResolver extends WafRestErrorResolver {
    private List<FriendlyExceptionMessageConverter> converters;

    /**
     * Sets converters.
     *
     * @param converters the converters
     */
    @Autowired
    public void setConverters(List<FriendlyExceptionMessageConverter> converters) {
        this.converters = converters;
    }

    @Override
    protected ResponseEntity<ResponseErrorMessage> process(Throwable throwable, HttpServletRequest request) {
        ResponseEntity<ResponseErrorMessage> responseEntity = super.process(throwable, request);
        for (FriendlyExceptionMessageConverter converter : converters) {
            if ( converter.convert(responseEntity, request) ) {
                break;
            }
        }
        return responseEntity;
    }
}

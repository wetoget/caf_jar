package com.cqt.a8.rest.exceptions.rest;

import com.cqt.a8.restclient.exception.entity.ResponseErrorMessage;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;


import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.UUID;

/**
 * 对特定的异常进行处理，并以 JSON 响应异常信息.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public abstract class AbstractRestErrorHandler {

    /**
     * Process response entity.
     *
     * @param throwable the throwable
     * @param request   the request
     * @return the response entity
     */
    public ResponseEntity<ResponseErrorMessage> process(Throwable throwable, HttpServletRequest request) {
        Assert.notNull(throwable, "throwable");
        Assert.notNull(request, "request");

        ResponseErrorMessage errorMessage = getBody(throwable, request);
        HttpHeaders httpHandlers = getHttpHandlers(throwable, request);
        HttpStatus httpStatus = getHttpStatus(throwable, request);
        return new ResponseEntity<>(errorMessage, httpHandlers, httpStatus);
    }

    /**
     * Gets body.
     *
     * @param throwable the throwable
     * @param request   the request
     * @return the body
     */
    protected ResponseErrorMessage getBody(Throwable throwable, HttpServletRequest request) {
        ResponseErrorMessage errorMessage = new ResponseErrorMessage(throwable);
        errorMessage.setMessage(throwable.getLocalizedMessage());
        errorMessage.setDetail(appendStackTrace(null, throwable));
        errorMessage.setCode(getCode(throwable, request));
        updateRemoteErrorMessage(errorMessage, request);
        return errorMessage;
    }

    /**
     * Update remote error message.
     *
     * @param errorMessage the error message
     * @param request      the request
     */
    protected void updateRemoteErrorMessage(ResponseErrorMessage errorMessage, HttpServletRequest request) {
        errorMessage.setServerTime(new Date());
        errorMessage.setHostId(request.getServerName());
        errorMessage.setRequestId(UUID.randomUUID().toString());

    }

    /**
     * Gets http status.
     *
     * @param throwable the throwable
     * @param request   the request
     * @return the http status
     */
    protected HttpStatus getHttpStatus(Throwable throwable, HttpServletRequest request) {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    /**
     * Gets code.
     *
     * @param throwable the throwable
     * @param request   the request
     * @return the code
     */
    protected String getCode(Throwable throwable, HttpServletRequest request) {
        return "CAF/INTERNAL_SERVER_ERROR";
    }

    /**
     * Gets http handlers.
     *
     * @param throwable the throwable
     * @param request   the request
     * @return the http handlers
     */
    protected HttpHeaders getHttpHandlers(Throwable throwable, HttpServletRequest request) {
        return null;
    }

    /**
     * Gets stack trace.
     *
     * @param throwable the throwable
     * @return the stack trace
     */
    protected String getStackTrace(Throwable throwable) {
        StringWriter errors = new StringWriter();
        throwable.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

    /**
     * Append stack trace string.
     *
     * @param detail    the detail
     * @param throwable the throwable
     * @return the string
     */
    protected String appendStackTrace(String detail, Throwable throwable) {
        if (throwable != null) {
            if (detail == null) {
                detail = "";
            } else {
                detail += "\r\n";
            }
            detail += "Stack trace:\r\n" + getStackTrace(throwable);
        }
        return detail;
    }
}

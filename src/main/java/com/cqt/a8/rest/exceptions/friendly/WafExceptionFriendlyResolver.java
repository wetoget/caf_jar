package com.cqt.a8.rest.exceptions.friendly;

import com.cqt.a8.restclient.WafException;
import com.cqt.a8.restclient.exception.entity.ResponseErrorMessage;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Waf exception friendly resolver.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018-04-25 10:56
 */
@Component
@Order(100)
public class WafExceptionFriendlyResolver extends FriendlyExceptionMessageConverter {
    @Override
    public boolean convert(ResponseEntity<ResponseErrorMessage> responseEntity, HttpServletRequest request) {
        Throwable throwable = responseEntity.getBody().getThrowable();

        return throwable instanceof WafException;
    }
}

package com.cqt.a8.rest.exceptions.friendly;

import com.cqt.a8.restclient.exception.entity.ResponseErrorMessage;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;


import javax.servlet.http.HttpServletRequest;

/**
 * The type Friendly exception message converter.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018-04-25 10:56
 */
public abstract class FriendlyExceptionMessageConverter {
    /**
     * Convert boolean.
     *
     * @param responseEntity the response entity
     * @param request        the request
     * @return the boolean
     */
    public abstract boolean convert(ResponseEntity<ResponseErrorMessage> responseEntity, HttpServletRequest request);

    /**
     * Update error message.
     *
     * @param responseEntity the response entity
     * @param message        the message
     */
    protected void updateErrorMessage(ResponseEntity<ResponseErrorMessage> responseEntity, String message) {
        ResponseErrorMessage errorMessage = responseEntity.getBody();

        String detail = "Message:" + errorMessage.getMessage();
        String srcDetail = errorMessage.getDetail();
        if (StringUtils.hasText(srcDetail)) {
            detail += "\r\n" + srcDetail;
        }
        errorMessage.setDetail(detail);
        errorMessage.setMessage(message);
    }
}

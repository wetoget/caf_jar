package com.cqt.a8.rest.exceptions.rest;

import com.cqt.a8.restclient.exception.entity.ResponseErrorMessage;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Illegal request method rest error handler.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class IllegalRequestMethodRestErrorHandler extends AbstractRestErrorHandler {

    private HttpStatus httpStatus;
    private String code;

    /**
     * Instantiates a new Illegal request method rest error handler.
     *
     * @param httpStatus the http status
     */
    public IllegalRequestMethodRestErrorHandler(HttpStatus httpStatus) {
        this(httpStatus, "CAF/" + httpStatus.getReasonPhrase().toUpperCase().replace(" ", "_"));
    }

    /**
     * Instantiates a new Illegal request method rest error handler.
     *
     * @param httpStatus the http status
     * @param code       the code
     */
    public IllegalRequestMethodRestErrorHandler(HttpStatus httpStatus, String code) {
        this.httpStatus = httpStatus;
        this.code = code;
    }

    /**
     * 处理spring不支持的请求方法，如PROPFIND、LOCK、LINK等
     * */
    @Override
    public ResponseEntity<ResponseErrorMessage> process(Throwable throwable, HttpServletRequest request) {
        Assert.notNull(throwable, "throwable");
        Assert.notNull(request, "request");

        HttpStatus status;
        ResponseErrorMessage errorMessage;
        if (null != throwable.getLocalizedMessage() && throwable.getLocalizedMessage().contains("org.springframework.http.HttpMethod")) {
            status = getHttpStatus(throwable, request);
            errorMessage = getBody(throwable, request);
        }else {
            status = super.getHttpStatus(throwable, request);
            errorMessage = super.getBody(throwable, request);
        }
        HttpHeaders httpHandlers = getHttpHandlers(throwable, request);

        return new ResponseEntity<>(errorMessage, httpHandlers, status);
    }

    @Override
    protected ResponseErrorMessage getBody(Throwable throwable, HttpServletRequest request) {
        ResponseErrorMessage errorMessage = new ResponseErrorMessage(throwable);
        errorMessage.setMessage("Unsupported request method");
        errorMessage.setDetail(appendStackTrace(null, throwable));
        errorMessage.setCode(code);
        updateRemoteErrorMessage(errorMessage, request);
        return errorMessage;
    }

    @Override
    protected HttpStatus getHttpStatus(Throwable throwable, HttpServletRequest request) {
        return httpStatus;
    }

}

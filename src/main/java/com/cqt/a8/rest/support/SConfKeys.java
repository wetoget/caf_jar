package com.cqt.a8.rest.support;

import com.cqt.a8.restclient.support.ConfKey;

/**
 * The enum S conf keys.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public enum SConfKeys implements ConfKey {

    /**
     * 是否转化远程处理异常http status
     * 默认为true进行转化(转为500)
     * false则是把原始http status透传给用户
     */
    CAF_EXCEPTION_REMOTE_HTTPSTATUS_PROXY("caf.exception.remote.httpstatus.proxy", "true"),
    /**
     * Caf exception friendly disabled s conf keys.
     */
    CAF_EXCEPTION_FRIENDLY_DISABLED("caf.exception.friendly.disabled", "true");

    private final String key;
    private final String defaultValue;
    SConfKeys(String key, String defaultValue){
        this.key = key;
        this.defaultValue = defaultValue;
    }

    @Override
    public String getKey(){
        return key;
    }

    @Override
    public String getDefaultValue(){
        return defaultValue;
    }
}

package com.cqt.a8.rest.config;

import com.cqt.a8.rest.exceptions.DefaultWafRestErrorResolver;
import com.cqt.a8.rest.exceptions.FriendlyWafRestErrorResolver;
import com.cqt.a8.rest.exceptions.WafErrorResolver;
import com.cqt.a8.rest.exceptions.WafRestErrorResolver;
import com.cqt.a8.rest.exceptions.rest.RestErrorMappings;
import com.cqt.a8.rest.filter.ExceptionFilter;
import com.cqt.a8.rest.support.SConfKeys;
import com.cqt.a8.restclient.properties.WafProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * The type Caf configuration.
 * @author: wangmingkui
 * @create: 2018-04-25 10:56
 */
@Configuration
@ComponentScan(basePackages = {"com.cqt.a8.rest.exceptions.friendly"})
public class CafConfiguration {
    /**
     * 具体执行异常处理
     *
     * @return waf error resolver
     */
    @Bean
    public WafErrorResolver wafErrorResolver() {
        WafRestErrorResolver resolver;
        if (Boolean.parseBoolean(WafProperties.getProperty(SConfKeys.CAF_EXCEPTION_FRIENDLY_DISABLED))) {
            resolver = new WafRestErrorResolver();
        } else {
            resolver = new FriendlyWafRestErrorResolver();
        }

        for (RestErrorMappings mapping : RestErrorMappings.values()) {
            resolver.addHandler(mapping.getThrowableClass(), mapping.getHandler());
        }

        return resolver;
    }

    /**
     * Default waf rest error resolver waf error resolver.
     *
     * @return the waf error resolver
     */
    @Bean
    public WafErrorResolver defaultWafRestErrorResolver() {
        return new DefaultWafRestErrorResolver();
    }

    /**
     * 对异常进行拦截 ServletFilter（为了支持 Spring bean）
     *
     * @return exception filter
     */
    @Bean
    public ExceptionFilter exceptionFilter() {
        return new ExceptionFilter();
    }

}



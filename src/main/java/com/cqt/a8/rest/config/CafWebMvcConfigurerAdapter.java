package com.cqt.a8.rest.config;


import com.cqt.a8.rest.filter.CafInterceptor;
import com.cqt.a8.restclient.util.WafJsonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.util.ClassUtils;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerExceptionResolverComposite;


import javax.xml.transform.Source;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * 基于spring mvc 注解方式进行配置WAF的spring配置。在本类中配置如下：<br>
 * 1、rest风格的数据转换器<br>
 * 2、异常配置资源加载<br>
 * 3、默认支持的ContentType<br>
 * 4、rest风格的异常处理器{@see RestHandlerExceptionResolver}<br>
 * 5、内部注解类的扫描路径配置<br>
 * @author: wangmingkui
 * @create: 2018-04-25 10:56
 */
@Configuration
@EnableWebMvc
@ComponentScan(value = "com.cqt.a8", includeFilters = @ComponentScan.Filter(Controller.class))
public class CafWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {
	
	private static boolean initFlag = false;
	
    private static final boolean jaxb2Present =
            ClassUtils.isPresent("javax.xml.bind.Binder", Thread.currentThread().getContextClassLoader());

    private static final boolean jackson2Present =
            ClassUtils.isPresent("com.fasterxml.jackson.databind.ObjectMapper", Thread.currentThread().getContextClassLoader()) &&
                    ClassUtils.isPresent("com.fasterxml.jackson.core.JsonGenerator", Thread.currentThread().getContextClassLoader());

    private static final boolean jacksonPresent =
            ClassUtils.isPresent("org.codehaus.jackson.map.ObjectMapper", Thread.currentThread().getContextClassLoader()) &&
                    ClassUtils.isPresent("org.codehaus.jackson.JsonGenerator", Thread.currentThread().getContextClassLoader());

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.favorPathExtension(false).favorParameter(false);
        configurer.defaultContentType(MediaType.APPLICATION_JSON);
    }

    /**
     * 转化支持的格式
     *
     * @return application /json
     */
    public MediaType converterMediaType(){
        return MediaType.APPLICATION_JSON;
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        // 默认非 UTF-8
        StringHttpMessageConverter stringConverter = new StringHttpMessageConverter(Charset.forName(CafConstants.UTF_8));
        stringConverter.setWriteAcceptCharset(false);

        converters.add(new ByteArrayHttpMessageConverter());
        converters.add(stringConverter);
        converters.add(new ResourceHttpMessageConverter());
        converters.add(new SourceHttpMessageConverter<Source>());
        converters.add(new AllEncompassingFormHttpMessageConverter());
        if (jaxb2Present) {
            converters.add(new Jaxb2RootElementHttpMessageConverter());
        }
        if (jackson2Present) {
            MappingJackson2HttpMessageConverter convert = new MappingJackson2HttpMessageConverter();
            convert.setObjectMapper(WafJsonMapper.getMapper());

            //重置媒体类型不带charset
            List<MediaType> supportedMediaTypes = new ArrayList<>();
            supportedMediaTypes.add(converterMediaType());
            convert.setSupportedMediaTypes(supportedMediaTypes);

            converters.add(convert);
        }
//        } else if (jacksonPresent) {
//            converters.add(new org.springframework.http.converter.json.MappingJacksonHttpMessageConverter());
//        }
        customMediaTypeSupport(converters);
    }

    /**
     * Custom media type support.
     *
     * @param converters the converters
     */
    public void customMediaTypeSupport(List<HttpMessageConverter<?>> converters) {
        //used for custom
    }

    /**
     * Multipart resolver multipart resolver.
     *
     * @return the multipart resolver
     */
    @Bean
    public MultipartResolver multipartResolver() {
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        resolver.setDefaultEncoding(CafConstants.UTF_8);
        resolver.setMaxUploadSize(5L * 1024 * 1024);
        resolver.setMaxInMemorySize(512 * 1024);
        return resolver;
    }


    /**
     * waf默认实现，全部异常由 {link com.xx.a8.rest.filter.ExceptionFilter} 接管，自定义注册的过滤器需在ExceptionFilter之后执行，确保ExceptionFilter能拦截到所有异常
     * 如果覆盖com.cqt.a8.AbstractCafWebApplicationInitializer#onStartup方法，请先调用super.onStartup(servletContext);确保先注册ExceptionFilter
     * 注意，{@link org.springframework.web.bind.annotation.ExceptionHandler @ExceptionHandler} 及 {@link org.springframework.web.bind.annotation.ResponseStatus @ResponseStatus} 将不能使用
     * @return
     */
    @Bean
    public HandlerExceptionResolver handlerExceptionResolver() {
        return new HandlerExceptionResolverComposite();
    }

    /**
     * 扩展使用,引用此框架的项目使用拦截器需要执行这个方法
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //registry.addInterceptor(CafInterceptor()).addPathPatterns("/*");
        //registry.addInterceptor(CafInterceptor()); 开启caf的拦截器
    }

    @Bean
    public CafInterceptor CafInterceptor(){
        return new CafInterceptor();
    }

    @Autowired(required = false)
    protected void throwExceptionIfNoHandlerFound(DispatcherServlet dispatcherServlet){
        dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
    }
}
package com.cqt.a8.rest.config;


/**
 * 常量
 * @author: wangmingkui
 * @create: 2018-04-25 10:56
 */
public class CafConstants {
	private CafConstants() {
	}

    /**
     * The constant UTF_8.
     */
    public static final String UTF_8 = "UTF-8";

    /**
     * The constant APPLICATION_JSON_UTF8.
     * 返回的Content-type为json/utf-8类型
     */
    public static final String APPLICATION_JSON_UTF8 = "application/json;charset=UTF-8";

}

package com.cqt.a8.rest.utils;

import com.cqt.a8.restclient.WafException;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

/**
 * 异常相关的类.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
public class ExceptUtil {
    private ExceptUtil() {
    }

    /**
     * Check value.
     *
     * @param check   the check
     * @param message the message
     */
    public static void checkValue(String check, String message){
        if (StringUtils.isEmpty(check)){
            throw new WafException("CAF/BAD_REQUEST", message, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Check not null.
     *
     * @param object  the object
     * @param message the message
     */
    public static void checkNotNull(Object object, String message) {
        if (null == object) {
            throw new WafException("CAF/BAD_REQUEST", message, HttpStatus.BAD_REQUEST);
        }
    }
}

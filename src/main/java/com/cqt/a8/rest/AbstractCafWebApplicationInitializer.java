
package com.cqt.a8.rest;

import com.cqt.a8.rest.filter.CafContextFilter;
import com.cqt.a8.rest.filter.CafCorsFilter;
import com.cqt.a8.rest.filter.CafHttpMethodOverrideFilter;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.*;
import javax.servlet.ServletRegistration.Dynamic;
import java.util.EnumSet;

/**
 * web项目的启动配置.
 *
 * @program: caf
 * @author: wangmingkui
 * @create: 2018 -04-25 10:56
 */
@Order(1)
public abstract class AbstractCafWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    /**
     * 設置当spring没有找到Handler的时候，抛出NoHandlerFoundException异常。并且被异常捕获到。统一进行处理
     */
    @Override
    protected void customizeRegistration(Dynamic registration) {
        registration.setInitParameter("throwExceptionIfNoHandlerFound", "true");
    }

    @Override
    public void onStartup(ServletContext servletContext)
            throws ServletException {

        //保持兼容性
//        WafProperties.getDefaultProperties().setProperty(SConfKeys.CAF_UC_REALM.getKey(), getRealm());
        super.onStartup(servletContext);
        initFilters(servletContext);
        registerFilters(servletContext);
    }
    
    private void initFilters(ServletContext servletContext) {

        initCharacterEncodingFilter(servletContext);
        addFilter(servletContext, "CafCorsFilter", new CafCorsFilter());
        addFilter(servletContext, "CafHttpMethodOverrideFilter", new CafHttpMethodOverrideFilter());
        addFilter(servletContext, "CafContextFilter", new CafContextFilter());
        addFilter(servletContext, "exceptionFilter", new DelegatingFilterProxy("exceptionFilter"));
    }

    /**
     * Register filters.
     *
     * @param servletContext the servlet context
     */
    protected void registerFilters(ServletContext servletContext) {
    }


    /**
     * Init character encoding filter.
     *
     * @param servletContext the servlet context
     */
    protected void initCharacterEncodingFilter(ServletContext servletContext) {
	    CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
	    //characterEncodingFilter.setForceEncoding(true);
	    characterEncodingFilter.setEncoding("UTF-8");
	    addFilter(servletContext, "characterEncodingFilter", characterEncodingFilter);
    }

    /**
     * Add filter.
     *
     * @param servletContext the servlet context
     * @param filterName     the filter name
     * @param filter         the filter
     */
    protected void addFilter(ServletContext servletContext, String filterName, Filter filter) {
        FilterRegistration.Dynamic filterRegistration = servletContext.addFilter(filterName, filter);
        filterRegistration.setAsyncSupported(isAsyncSupported());
        filterRegistration.addMappingForUrlPatterns(getDispatcherTypes(), false, "/*");
    }

    /**
     * Gets dispatcher types.
     *
     * @return the dispatcher types
     */
    protected EnumSet<DispatcherType> getDispatcherTypes() {
        if (isAsyncSupported()) {
            return EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.INCLUDE, DispatcherType.ASYNC);
        } else {
            return EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.INCLUDE);
        }
    }

    /**
     * 默认服务启动，加载的权限验证模块实现类
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{};
    }

}
